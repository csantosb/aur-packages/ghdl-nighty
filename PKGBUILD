# Maintainer: Filipe Laíns (FFY00) <lains@archlinux.org>

pkgbase=ghdl
pkgname=('ghdl-llvm' 'ghdl-gcc')
_gcc=10.2.0
_isl=0.24
pkgver=nightly
pkgrel=1
pkgdesc='VHDL simulator'
arch=('x86_64')
url='https://github.com/ghdl/ghdl'
license=('GPL2')
depends=('gcc-ada')
makedepends=('gmp' 'mpfr' 'libmpc' 'zlib' 'llvm' 'clang' 'diffutils')
validpgpkeys=('33C235A34C46AA3FFB293709A328C3A2C3C45C06'  # Jakub Jelinek <jakub@redhat.com>
              '13975A70E63C361C73AE69EF6EEB81F8981C74C7') # richard.guenther@gmail.com
source=("$pkgbase-$pkgver.tar.gz::$url/archive/$pkgver.tar.gz"
	"https://ftp.gnu.org/gnu/gcc/gcc-$_gcc/gcc-$_gcc.tar.xz"{,.sig}
	"https://gcc.gnu.org/pub/gcc/infrastructure/isl-${_isl}.tar.bz2")
sha512sums=('SKIP'
	    'SKIP'
	    'SKIP'
	    'SKIP')

prepare() {
  cd "$srcdir"

  # cd $pkgbase-$pkgver
  # patch -p1 -i ../fix-llvm11.patch
  # cd ..

  # cp -r $pkgbase-$pkgver ghdl-mcode
  cp -r $pkgbase-$pkgver ghdl-llvm
  cp -r $pkgbase-$pkgver ghdl-gcc
  # cp -r $pkgbase-$pkgver pyghdl

  mkdir gcc-build

  cd gcc-$_gcc

  ln -s ../isl-$_isl isl

  echo $_gcc > gcc/BASE-VER

  # hack! - some configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" {libiberty,gcc}/configure
}

_configure() {
  ./configure \
      --prefix=/usr \
      --disable-werror \
      --enable-checks \
      --enable-libghdl \
      --enable-synth $@
# Note : Add --enable-openieee to use free (but not complete) implementation of IEEE VHDL libs
# --enable-openieee \
}

build() {
  export GNATMAKE="gnatmake $MAKEFLAGS"

  # echo 'Building ghdl-mcode...'
  # cd "$srcdir"/ghdl-mcode

  # _configure

  # make

  echo 'Building ghdl-llvm...'
  cd "$srcdir"/ghdl-llvm

  _configure --with-llvm-config

  make

  echo 'Building ghdl-gcc...'
  cd "$srcdir"/ghdl-gcc

  _configure --with-gcc="$srcdir"/gcc-$_gcc

  make copy-sources

  cd "$srcdir"/gcc-build

  "$srcdir"/gcc-$_gcc/configure \
      --prefix=/usr \
      --libdir=/usr/lib \
      --libexecdir=/usr/lib \
      --enable-languages=vhdl \
      --enable-default-pie \
      --enable-default-ssp \
      --with-isl \
      --enable-plugin \
      --enable-lto \
      --disable-bootstrap \
      --disable-multilib \
      --disable-libada \
      --disable-libsanitizer \
      --disable-libssp \
      --disable-libquadmath \
      --disable-libgomp \
      --disable-libvtv \
      --with-pkgversion='Arch Linux Repositories' \
      --with-bugurl='https://bugs.archlinux.org/'

  make

  cd "$srcdir"/ghdl-gcc

  make \
      GHDL_GCC_BIN="$srcdir"/gcc-build/gcc/ghdl \
      GHDL1_GCC_BIN="--GHDL1=$srcdir/gcc-build/gcc/ghdl1" \
      ghdllib

  # echo 'Building pyghdl...'
  # cd "$srcdir"/pyghdl

  # python setup.py build
}

#check() {
#  cd $pkgbase-$pkgver/testsuite
#
#  echo 'Running tests for ghdl-mcode...'
#  GHDL="$srcdir"/ghdl-mcode/ghdl_mcode ./testsuite.sh
#
#  echo 'Running tests for ghdl-llvm...'
#  GHDL="$srcdir"/ghdl-llvm/ghdl1-llvm ./testsuite.sh
#
#  echo 'Running tests for ghdl-gcc...'
#  GHDL="$srcdir"/gcc-build/gcc/ghdl ./testsuite.sh
#}

# package_ghdl-mcode() {
#   pkgdesc="$pkgdesc (mcode backend)"
#   provides=('ghdl')
#   conflicts=('ghdl')

#   cd $pkgname

#   make DESTDIR="$pkgdir" install
# }

package_ghdl-llvm() {
  pkgdesc="$pkgdesc (LLVM backend)"
  depends+=('llvm-libs')
  provides=('ghdl')
  conflicts=('ghdl')
  options=(!emptydirs)

  cd $pkgname

  make DESTDIR="$pkgdir" install

  # strip binaries
  find "$pkgdir"/usr/bin/ -type f -and \( -executable -o -name '*.o' \) -exec strip '{}' \;
}

package_ghdl-gcc() {
  pkgdesc="$pkgdesc (GCC backend)"
  provides=('ghdl')
  conflicts=('ghdl')
  options=(!emptydirs)

  cd "$srcdir"/gcc-build

  make DESTDIR="$pkgdir" install

  # Remove files that conflict with the system gcc -- we only want to install the ghdl plugin
  for FILE in $(find "$pkgdir" -not -type d); do
    if [ -f /"${FILE#"$pkgdir"}" ]; then
      rm -f "$FILE"
    fi
  done

  find "$pkgdir"/usr/share -not -type d -not -name '*ghdl*' -delete
  rm -rf "$pkgdir"/usr/lib64

  # strip binaries
  find "$pkgdir"/usr/bin/ "$pkgdir"/usr/lib/gcc -type f -and \( -executable -o -name '*.o' \) -exec strip '{}' \;

  cd "$srcdir"/ghdl-gcc

  make DESTDIR="$pkgdir" install
}

# package_python-pyghdl() {
#   pkgdesc='Python bindings for GHDL'
#   depends=('ghdl' 'python-pydecor' 'python-pyvhdlmodel')

#   cd pyghdl

#   python setup.py install --root="$pkgdir" --optimize=1 --skip-build
# }
